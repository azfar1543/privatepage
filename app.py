from flask import Flask,render_template,request,Response,redirect
from flask_sslify import SSLify
from functools import wraps
from flask_static_compress import FlaskStaticCompress
app = Flask(__name__)
sslify = SSLify(app)
def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == 'cloud' and password == 'meta'

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated
def check(input_text):
    if input_text == "signup" or input_text == "sign" or input_text == "signu":
        return redirect('signup')
    elif input_text == 'hackphs' or input_text == 'hackphs':
        return redirect('/photos/2019/hackPHS/')
    elif input_text == 'gallery' or input_text == 'pictures':
        return redirect('gallery')
    elif input_text == "admin":
        return redirect('admin')
    elif input_text == "detail":
        return redirect('detail')
    elif input_text == "results":
        return redirect('results')
    elif input_text == "qbhacks":
        return redirect('/photos/2019/QBHacks/')
    elif input_text == "hackja":
        return redirect('/photos/2019/hackJA/')
    elif input_text == "about" or input_text == "abou" or input_text == "abo":
        return redirect('about')
    elif input_text == "gallery" or input_text == "galler" or input_text == "gall":  
        return redirect('gallery')
    elif input_text == "interview" or input_text == "intervie" or input_text == "intervi":
        return redirect('interview')
    elif input_text == 'team meta':
        return redirect('')
    else:
        return "Query not found!"
@app.route('/admin')
@requires_auth
def admin():
    return render_template('secret.html')
@app.errorhandler(404)
def error(e):
    return render_template('404.html'),404
app.register_error_handler(404, error)
compress = FlaskStaticCompress(app)
dictionary = [
    {'key':'JWzLnorFde','score':'85/125'},
    {'key':'cILFJKXfJI','score':'90/125'},
    {'key':'LMIWG3J4aU','score':'105/125'},
    {'key':'An7tBtrfs3','score':'110/125'},
    {'key':'6NGy31Aarw','score':'98/125'},
    {'key':'GTcEGesOcR','score':'94/125'},
    {'key':'6SN90JI5jf','score':'104/125'},
    {'key':'8bB4BuwNjr','score':'76/125'},
    {'key':'ZD3cdMYGAa','score':'86/125'},
    {'key':'BBpHSPkEIk','score':'120/125'},
    {'key':'AZ4LK7dc1o','score':'104/125'},
    {'key':'dQxq7OGLC6','score':'105/125'},
    {'key':'69EXvzEZzG','score':'106/125'},
    {'key':'F3rwywyPJg','score':'115/125'},
    {'key':'KmQGFpVQ7X','score':'97/125'},
    {'key':'CZjnm1sLGg','score':'95/125'}
    ]
@app.route('/')
def home():
    if request.args.get('input'):
        input_text = request.args.get('input').lower()
        return check(input_text)
    return render_template('home.html')
@app.route('/about')
def about():
    return render_template('about.html')
@app.route('/signup')
def signup():
    return render_template('signup.html')
@app.route('/photos/2019/hackJA/')
def photos():
    return render_template('photos.html')
@app.route('/results')
def results():
    return render_template('results.html',results=dictionary,len=len(dictionary))
@app.route('/photos/2019/hackPHS/')
def hackphs():
    return render_template('hackphs.html')
@app.route('/photos/2019/QBHacks/')
def qbhacks():
    return render_template('qbhacks.html')
@app.route('/gallery/')
def gallery():
    return render_template('gallery.html')
@app.route('/detail',methods=['POST','GET'])
def detail():
    if request.method == "POST":
        key = request.form['key']
        password = request.form['password']
        if key == "An7tBtrfs3" and password == 'B04qM5KOBX':
            return render_template('detailedview.html',user='Rohan')
        elif key == '6NGy31Aarw' and password == "DbDAQeup4P":
            return render_template('detailedview.html',user="Aarav")
        else:
            return "Incorrect Key or Password"
    return render_template('detail.html')
@app.route('/interview',methods = ['GET','POST'])
def interview():
    if request.method == "POST":
        if request.form['password'] == 'rB8T51e4wb':
            return render_template('interviewsign.html')
        else:
            return "Wrong Access Password"
            
    return render_template('interview.html')
@app.route('/projects')
def projects():
    return render_template('projects.html')
@app.route('/slideshow')
def slideshow():
    return render_template('slideshow.html')
if __name__ == "__main__":
    app.run(port=10444,debug=True,ssl_context='adhoc')
    